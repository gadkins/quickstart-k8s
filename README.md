# Uffizzi Quickstart for Kubernetes (~ 1 minute)

Go from pull request to Uffizzi Ephemeral Environment in less than one minute. This quickstart will create a virtual Kubernetes cluster on Uffizzi Cloud and deploy a sample microservices application from this repository. Once created, you can connect to the cluster with the Uffizzi CLI, then manage the cluster via `kubectl`, `kustomize`, `helm`, and other tools. You can clean up the cluster by closing the pull request or manually deleting it via the Uffizzi CLI.

### 1. Fork this `quickstart-k8s` repo

[https://gitlab.com/uffizzicloud/quickstart-k8s/-/forks/new](https://gitlab.com/uffizzicloud/quickstart-k8s/-/forks/new)


### 2. Fork settings

After clicking the fork link, under project URL select your username as the namespace and `Public` as the visibility level.

<img src="misc/images/step2.png" width="800">

<details>
  <summary>
    <h3>
      3. Enable commenting (optional)
    </h3>
  </summary>
  Optionally, you can configure the GitLab Notes API to post the Ephemeral Environment URL as a comment in your merge request. To enable this feature, follow these steps:

__Step 1__: Create a GitLab Access Token
To interact with the GitLab Notes API, you'll need to create a GitLab access token with the appropriate permissions. Go to Settings in the forked repository and navigate to `Access Tokens`. Create a new token with `api` scope and `Developer` role.

<img src="misc/images/PAT.png" width="800">

__Step 2__: Add the newly created Access Token as a CI/CD Variable named `CI_API_TOKEN`. To do this, within to your forked repository on GitLab. Navigate to Settings > CI/CD > Variables. Add a new variable with the name `CI_API_TOKEN` and paste your access token as the value.

<img src="misc/images/var.png" width="600">


__Step 3__: When the Gitlab pipeline is done executing, you will see a comment on the MR with details regarding the next steps to take in order to access the newly created uCluster. Use the result and vote endpoints to access the web app.

<img src="misc/images/comm.png" width="600">
</details>

### 4. Open a merge request for the `try-uffizzi` branch against `main` in your fork

⚠️ Be sure that you're opening a MR on the branches of _your fork_ (i.e. Source branch: `your-account/quickstart-k8s/try-uffizzi`, Target branch: `your-account/quickstart-k8s/main`).

If you try to open a MR for `uffizzicloud/main` ← `your-account/try-uffizzi`, the triggered pipeline will not run in this example.

<img src="misc/images/Step3.png" width="800">

### 5. View cluster details
Once the pipeline is finished running, check the pipeline logs to find information on how to access the newly created cluster. Within the MR, open up the `Pipelines` tab to find the running pipelines.

___
## What to expect

The MR will trigger a [Gitlab Pipeline workflow](.gitlab-ci.yml) that uses the Uffizzi CLI, and Kubernetes manifests to create a Uffizzi Ephemeral Environment for the [microservices application](#architecture-of-this-example-app) defined by this repo. When the workflow completes, the Ephemeral Environment URL will be posted as a comment in your PR issue.

## How it works

### Configuration

Ephemeral Environments are configured with [Kubernetes manifests](manifests/) that describe the application components and a [Gitlab Pipeline workflow](.gitlab-ci.yml) that includes a series of jobs triggered by a `merge_request` event and subsequent `push` events:

1. [Build and push the voting-app images](https://gitlab.com/uffizzicloud/quickstart-k8s/-/blob/main/.gitlab-ci.yml?ref_type=heads#L31-L53)
2. [Create the Uffizzi cluster using the uffizzi-cli](https://gitlab.com/uffizzicloud/quickstart-k8s/-/blob/main/cluster.gitlab-ci.yml?ref_type=heads#L40)
3. [Apply the Kubernetes manifests to deploy the application to the Uffizzi cluster](https://gitlab.com/uffizzicloud/quickstart-k8s/-/blob/main/.gitlab-ci.yml?ref_type=heads#L70)
4. [Delete the Ephemeral Environment when the PR is merged/closed or after](https://gitlab.com/uffizzicloud/quickstart-k8s/-/blob/main/cluster.gitlab-ci.yml?ref_type=heads#L59-L87)

## Acceptable Use

We strive to keep Uffizzi Cloud free or inexpensive for individuals and small teams. Therefore, activities such as crypto mining, file sharing, bots, and similar uses that lead to increased costs and intermittent issues for other users are strictly prohibited per the [Acceptable Use Policy](https://www.uffizzi.com/legal/acceptable-use-policy). Violators of this policy are subject to permanent ban.

## Architecture of this Example App

The application defined by this repo allows users to vote for dogs or cats and see the results. It consists of the following microservices:

<img src="https://user-images.githubusercontent.com/7218230/192601868-562b705f-bf39-4eb8-a554-2a0738bd8ecf.png" width="400">

* `voting` - A frontend web app in [Python](/vote) that lets you vote between two options
* `redis` - A [Redis](https://hub.docker.com/_/redis/) queue that collects new votes
* `worker` - A [.NET Core](/worker/src/Worker) worker that consumes votes and stores them in...
* `db` - A [PostgreSQL](https://hub.docker.com/_/postgres/) database backed by a Docker volume
* `result` - A [Node.js](/result) web app that shows the results of the voting in real time
